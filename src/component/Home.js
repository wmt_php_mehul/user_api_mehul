import React, { Component } from 'react'
import axios from 'axios';
import Registration from './Registration';
import 'bootstrap/dist/css/bootstrap.css';

export class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: [],
            me: false
        };
        this.clickHandle = this.clickHandle.bind(this);
        this.getuser = this.getuser.bind(this);

    }

    componentDidMount() {
        this.getuser();
    }
    getuser() {
        axios({
            method: 'get',
            url: 'http://68.183.48.101:3333/users/list',
            headers: {
                Authorization: 'Bearer ' + this.props.token
            },
        }).then(res => this.setState({ user: res.data.data.users, me: false }));
    }

    clickHandle() {
        axios({
            method: 'get',
            url: 'http://68.183.48.101:3333/me',
            headers: {
                Authorization: 'Bearer '+this.props.token
            },
        }).then(res => this.setState({ user: res.data, me: true }));

    }
    render() {

        return (

            <div className='container '>
                <button className='btn btn-danger mt-1 mb-5' onClick={this.clickHandle}>Me</button>
                <button className='btn btn-danger mt-1 mb-5 ml-5' onClick={this.getuser}>User List</button>
                {console.log(this.state.me)}
                <div className='row'>
                    {(this.state.me) ? <div class="col-xs-12 col-sm-6 col-md-6">

                        <div class="row user">
                            <div class="col-sm-3 col-md-3">
                                <img src={this.state.user.profile_pic} alt="" class="img-rounded img-responsive h-50" />
                            </div>
                            <div class="col-sm-6 col-md-8">
                                <h4>
                                    {this.state.user.username}</h4>
                                <p>
                                    <i class="glyphicon glyphicon-envelope"></i>{this.state.user.email}
                                    <br />
                                    <br />
                                    <i class="glyphicon glyphicon-gift"></i>{this.state.user.created_at}</p>
                            </div>
                        </div>
                    </div>
                        : this.state.user.map((user, i) => {
                            return <div class="col-xs-12 col-sm-6 col-md-6">

                                <div class="row user">
                                    <div class="col-sm-3 col-md-3">
                                        <img src={user.profile_pic} alt="" class="img-rounded img-responsive h-50" />
                                    </div>
                                    <div class="col-sm-6 col-md-8">
                                        <h4>
                                            {user.username}</h4>
                                        <p>
                                            <i class="glyphicon glyphicon-envelope"></i>{user.email}
                                            <br />
                                            <br />
                                            <i class="glyphicon glyphicon-gift"></i>{user.created_at}</p>
                                    </div>
                                </div>
                            </div>
                        })}

                </div>
            </div>

        )
    }
}

export default Home
