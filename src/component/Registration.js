import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import Home from './Home'
export class Registration extends Component {
    state = {
        redirect: false,
        token:'',
      }
    register = (e) => {
        e.preventDefault();
        console.log(e.target.username.value)
        console.log(e.target.email.value)
        console.log(e.target.password.value)

        axios({
            method:'post',
            url: 'http://68.183.48.101:3333/users/register',
            data: {
                username: e.target.username.value,
                email: e.target.email.value,
                password: e.target.password.value
            },
        }).then(res => this.setState({token: res.data.data.token.token, redirect: true}));

     
    }

    render() {
      
      

     if (this.state.redirect) {
       return <Home token={this.state.token}/>
      }else 
      {
          return (
            
            <div className="container ">
                <div className="list-group w-100 p-3">
                    <form onSubmit={this.register}>
                        <input type="text" name="username" placeholder="username" className="list-group-item w-50 p-3 m-2" />
                        <input type="text" name="email" placeholder="Email" className="list-group-item  w-50 p-3 m-2" />

                        <input type="password" name="password" placeholder="Password" className="list-group-item  w-50 p-3 m-2" />

                        <input type='submit' className='btn btn-success  w-25 p-3 m-2 col-md-2  ' value='Sign Up' />
                    
                    </form>
                </div>
            </div>
      
        )
      }
    }
}

export default Registration
